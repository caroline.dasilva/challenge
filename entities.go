package main

import "time"

type Bank struct {
	ATMs []ATM
}

type ATM struct {
	Id           int
	Available    bool
	Cash         map[string]int
	LastWithdraw LastWithdraw
}

type Withdraw struct {
	ATMId int
	Value int
	Time  time.Time
}

type LastWithdraw struct {
	Value int
	Time  time.Time
}

// helpers

type InputAbastecimento struct {
	Caixa CaixaObject `json:"caixa"`
}

type CaixaObject struct {
	Identificador   int            `json:"identificador"`
	CaixaDisponivel bool           `json:"caixaDisponivel"`
	Notas           map[string]int `json:"notas"`
}

type InputSaque struct {
	Saque Saque `json:"saque"`
}

type Saque struct {
	CaixaId int    `json:"caixaID"`
	Valor   int    `json:"valor"`
	Horario string `json:"horario"`
}

type Output struct {
	Caixa CaixaObject `json:"caixa"`
	Erros []string    `json:"erros"`
}
