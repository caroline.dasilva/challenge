package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("hello world")

	bank := initBank(1)
	fmt.Println("bank before: ", bank)

	inputSupply, _ := getInputSupply(2, 100, 50, 10, 30, false)
	output := bank.Supply(inputSupply)
	fmt.Println("output: ", string(output))
	fmt.Printf("bank after: %v\n\n", bank)

	inputSupply, _ = getInputSupply(1, 100, 50, 10, 30, true)
	output = bank.Supply(inputSupply)
	fmt.Println("output: ", string(output))
	fmt.Printf("bank after: %v\n\n", bank)

	inputSupply, _ = getInputSupply(1, 100, 50, 10, 30, false)
	output = bank.Supply(inputSupply)
	fmt.Println("output: ", string(output))
	fmt.Printf("bank after: %v\n\n", bank)

	inputWithdraw, _ := getInputWithdraw(1, 50, 10, 10)
	output = bank.Withdraw(inputWithdraw)
	fmt.Println("output: ", string(output))
	fmt.Printf("bank after: %v\n\n", bank)

	inputWithdraw, _ = getInputWithdraw(1, 40, 10, 19)
	output = bank.Withdraw(inputWithdraw)
	fmt.Println("output: ", string(output))
	fmt.Printf("bank after: %v\n\n", bank)
}

func initBank(quantity int) Bank {
	var atms []ATM
	for i := 1; i <= quantity; i++ {
		newATM := ATM{
			Id:        i,
			Available: false,
			Cash: map[string]int{
				"notasDez":       0,
				"notasVinte":     0,
				"notasCinquenta": 0,
				"notasCem":       0,
			},
		}
		atms = append(atms, newATM)
	}
	bank := Bank{
		ATMs: atms,
	}
	return bank
}

func (bank *Bank) Supply(input ATM) []byte {
	fmt.Println("supplying ", input)

	index := -1
	var atm ATM
	errors := []string{}

	for i, item := range bank.ATMs {
		if item.Id == input.Id {
			index = i
			break
		}
	}

	if index == -1 {
		fmt.Printf("No ATM with ID %v found\n", input.Id)
		errors = append(errors, "caixa-inexistente")
		return formatOutput(input, errors)
	}

	atm = bank.ATMs[index]

	if atm.Available {
		fmt.Printf("ATM %v already available for customer use\n", input.Id)
		errors = append(errors, "caixa-em-uso")
		return formatOutput(input, errors)
	}

	for key, value := range input.Cash {
		atm.Cash[key] += value
	}
	atm.Available = input.Available

	bank.ATMs[index] = atm

	return formatOutput(atm, errors)
}

func (bank *Bank) Withdraw(input Withdraw) []byte {
	fmt.Println("withdrawing: ", input)

	index := -1
	var atm ATM
	errors := []string{}

	for i, item := range bank.ATMs {
		if item.Id == input.ATMId {
			index = i
			break
		}
	}

	if index == -1 {
		fmt.Printf("No ATM with ID %v found\n", input.ATMId)
		errors = append(errors, "caixa-inexistente")
		return formatOutput(atm, errors)
	}

	atm = bank.ATMs[index]

	if !atm.Available {
		fmt.Printf("ATM %v not available for customer use yet\n", input.ATMId)
		errors = append(errors, "caixa-indisponivel")
		return formatOutput(atm, errors)
	}

	if atm.totalValue() < input.Value {
		fmt.Printf("ATM %v does not have enough cash for withdraw of %v\n", input.ATMId, input.Value)
		errors = append(errors, "valor-indisponivel")
		return formatOutput(atm, errors)
	}

	tenMinutesLimit := atm.LastWithdraw.Time.Add(time.Minute * 10)
	if input.Value == atm.LastWithdraw.Value && input.Time.Before(tenMinutesLimit) {
		fmt.Println("It is not allowed to double withdraw within a gap of 10 minutes")
		errors = append(errors, "saque-duplicado")
		return formatOutput(atm, errors)
	}

	result := atm.calculateWithdraw(input.Value)
	for key := range atm.Cash {
		atm.Cash[key] -= result[key]
	}
	atm.LastWithdraw.Time = input.Time
	atm.LastWithdraw.Value = input.Value

	bank.ATMs[index] = atm

	return formatOutput(atm, errors)
}

func (atm *ATM) calculateWithdraw(value int) map[string]int {
	withdrawedCash := map[string]int{
		"notasDez":       0,
		"notasVinte":     0,
		"notasCinquenta": 0,
		"notasCem":       0,
	}

	aux := 0
	quantity, rest := value/100, value%100
	if atm.Cash["notasCem"] > quantity {
		withdrawedCash["notasCem"] = quantity
		aux = rest
	} else {
		rest = aux
	}

	quantity, rest = rest/50, rest%50
	if atm.Cash["notasCinquenta"] > quantity {
		withdrawedCash["notasCinquenta"] = quantity
		aux = rest
	} else {
		rest = aux
	}

	quantity, rest = rest/20, rest%20
	if atm.Cash["notasVinte"] > quantity {
		withdrawedCash["notasVinte"] = quantity
		aux = rest
	} else {
		rest = aux
	}

	quantity, rest = rest/10, rest%10
	if atm.Cash["notasDez"] > quantity {
		withdrawedCash["notasDez"] = quantity
		aux = rest
	} else {
		rest = aux
	}

	return withdrawedCash
}

func (atm *ATM) totalValue() int {
	return atm.Cash["notasDez"]*10 +
		atm.Cash["notasVinte"]*20 +
		atm.Cash["notasCinquenta"]*50 +
		atm.Cash["notasCem"]*100
}
