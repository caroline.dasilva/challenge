package main

import (
	"encoding/json"
	"fmt"
	"time"
)

func getInputSupply(id, dez, vinte, cinquenta, cem int, available bool) (ATM, string) {
	input := fmt.Sprintf(`{
		"caixa":{
		   "identificador":%v,
		   "caixaDisponivel":%v,
		   "notas":{
			  "notasDez":%v,
			  "notasVinte":%v,
			  "notasCinquenta":%v,
			  "notasCem":%v
		   }
		}
	 }`, id, available, dez, vinte, cinquenta, cem)
	atm := formatInputSupply(input)
	return atm, input
}

func getInputWithdraw(id, valor, hora, minuto int) (Withdraw, string) {
	input := fmt.Sprintf(`{
		"saque":{
		   "caixaID":%v,
		   "valor":%v,
		   "horario":"2022-04-01T%v:%v:00.000Z"
		}
	 }`, id, valor, hora, minuto)
	withdraw := formatInputWithdraw(input)
	return withdraw, input
}

func formatInputSupply(data string) ATM {
	var result ATM
	var input InputAbastecimento

	err := json.Unmarshal([]byte(data), &input)
	if err != nil {
		fmt.Println("Error unmarshaling json: ", err)
	}

	result.Id, result.Available, result.Cash = input.Caixa.Identificador, input.Caixa.CaixaDisponivel, input.Caixa.Notas

	return result
}

func formatInputWithdraw(data string) Withdraw {
	var result Withdraw
	var input InputSaque

	err := json.Unmarshal([]byte(data), &input)
	if err != nil {
		fmt.Println("Error unmarshaling json: ", err)
		panic(err)
	}

	time, err := time.Parse("2006-01-02T15:04:05.000Z", input.Saque.Horario)
	if err != nil {
		fmt.Println("Error parsing time")
		panic(err)
	}
	result.ATMId, result.Value, result.Time = input.Saque.CaixaId, input.Saque.Valor, time

	return result
}

func formatOutput(data ATM, erros []string) []byte {
	caixa := CaixaObject{}
	caixa.Identificador, caixa.CaixaDisponivel, caixa.Notas = data.Id, data.Available, data.Cash

	output := Output{
		Caixa: caixa,
		Erros: erros,
	}

	json, err := json.Marshal(output)
	if err != nil {
		fmt.Println("Error marshaling json: ", err)
	}
	return json
}
