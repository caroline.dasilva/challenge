package main

import (
	"bytes"
	"testing"
	"time"
)

func TestSupply_AtmDoesNotExist_ShouldReturnError(t *testing.T) {
	bank := initBank(1)

	input, _ := getInputSupply(2, 100, 50, 10, 30, false)

	output := formatOutput(input, []string{"caixa-inexistente"})
	expected := []byte(output)

	test := struct {
		name     string
		bank     Bank
		input    ATM
		want     []byte
		wantFail bool
	}{
		name:     "Attempt to supply non existent ATM should return error",
		bank:     bank,
		input:    input,
		want:     expected,
		wantFail: true,
	}

	t.Run(test.name, func(t *testing.T) {

		got := test.bank.Supply(test.input)
		equal := bytes.Equal(got, test.want)
		if !equal {
			t.Errorf("TestSupply_AtmDoesNotExist_ShouldReturnError result is not equal to expected")
		}
	})
}

func TestSupply_AtmAlreadyAvailable_ShouldReturnError(t *testing.T) {
	bank := initBank(1)
	bank.ATMs[0].Available = true

	input, _ := getInputSupply(1, 100, 50, 10, 30, false)

	output := formatOutput(input, []string{"caixa-em-uso"})
	expected := []byte(output)

	test := struct {
		name     string
		bank     Bank
		input    ATM
		want     []byte
		wantFail bool
	}{
		name:     "Attempt to supply already available ATM should return error",
		bank:     bank,
		input:    input,
		want:     expected,
		wantFail: true,
	}

	t.Run(test.name, func(t *testing.T) {

		got := test.bank.Supply(test.input)
		equal := bytes.Equal(got, test.want)
		if !equal {
			t.Errorf("TestSupply_AtmAlreadyAvailable_ShouldReturnError result is not equal to expected")
		}
	})
}

func TestSupply_SupplyCashValues_ShouldSucceed(t *testing.T) {
	bank := initBank(1)

	input, _ := getInputSupply(1, 100, 50, 10, 30, false)

	output := formatOutput(input, []string{})
	expected := []byte(output)

	test := struct {
		name     string
		bank     Bank
		input    ATM
		want     []byte
		wantFail bool
	}{
		name:     "Attempt to supply cash values ATM should update bank`s values correctly",
		bank:     bank,
		input:    input,
		want:     expected,
		wantFail: true,
	}

	t.Run(test.name, func(t *testing.T) {

		got := test.bank.Supply(test.input)
		equal := bytes.Equal(got, test.want)
		if !equal {
			t.Errorf("TestSupply_SupplyCashValues_ShouldSucceed result is not equal to expected")
		}

		for key, value := range bank.ATMs[0].Cash {
			if value != input.Cash[key] {
				t.Errorf("TestSupply_SupplyCashValues_ShouldSucceed bank value for %v does not match supplied value", key)
			}
		}
	})
}

func TestSupply_ChangeAtmAvailability_ShouldSucceed(t *testing.T) {
	bank := initBank(1)

	input, _ := getInputSupply(1, 100, 50, 10, 30, true)

	output := formatOutput(input, []string{})
	expected := []byte(output)

	test := struct {
		name     string
		bank     Bank
		input    ATM
		want     []byte
		wantFail bool
	}{
		name:     "Attempt to change ATM`s availability should update bank`s values correctly",
		bank:     bank,
		input:    input,
		want:     expected,
		wantFail: true,
	}

	t.Run(test.name, func(t *testing.T) {

		got := test.bank.Supply(test.input)
		equal := bytes.Equal(got, test.want)
		if !equal {
			t.Errorf("TestSupply_ChangeAtmAvailability_ShouldSucceed result is not equal to expected")
		}

		if bank.ATMs[0].Available != input.Available {
			t.Errorf("TestSupply_ChangeAtmAvailability_ShouldSucceed banks`s ATM availability should be %v but it is %v", bank.ATMs[0].Available, input.Available)
		}
	})
}

func TestWithdraw_AtmDoesNotExist_ShouldReturnError(t *testing.T) {
	bank := initBank(1)

	input, _ := getInputWithdraw(2, 50, 10, 10)

	atm := ATM{}
	output := formatOutput(atm, []string{"caixa-inexistente"})
	expected := []byte(output)

	test := struct {
		name     string
		bank     Bank
		input    Withdraw
		want     []byte
		wantFail bool
	}{
		name:     "Attempt to withdraw from non existent ATM should return error",
		bank:     bank,
		input:    input,
		want:     expected,
		wantFail: true,
	}

	t.Run(test.name, func(t *testing.T) {

		got := test.bank.Withdraw(test.input)
		equal := bytes.Equal(got, test.want)
		if !equal {
			t.Errorf("TestWithdraw_AtmDoesNotExist_ShouldReturnError result is not equal to expected")
		}
	})
}

func TestWithdraw_AtmNotAvailable_ShouldReturnError(t *testing.T) {
	bank := initBank(1)

	input, _ := getInputWithdraw(1, 50, 10, 10)

	output := formatOutput(bank.ATMs[0], []string{"caixa-indisponivel"})
	expected := []byte(output)

	test := struct {
		name     string
		bank     Bank
		input    Withdraw
		want     []byte
		wantFail bool
	}{
		name:     "Attempt to withdraw from non available ATM should return error",
		bank:     bank,
		input:    input,
		want:     expected,
		wantFail: true,
	}

	t.Run(test.name, func(t *testing.T) {

		got := test.bank.Withdraw(test.input)
		equal := bytes.Equal(got, test.want)
		if !equal {
			t.Errorf("TestWithdraw_AtmNotAvailable_ShouldReturnError result is not equal to expected")
		}
	})
}

func TestWithdraw_NotEnoughCashAvailable_ShouldReturnError(t *testing.T) {
	bank := initBank(1)
	bank.ATMs[0].Cash = map[string]int{
		"notasDez":       100,
		"notasVinte":     50,
		"notasCinquenta": 10,
		"notasCem":       100,
	}
	bank.ATMs[0].Available = true

	input, _ := getInputWithdraw(1, 50000, 10, 10)

	output := formatOutput(bank.ATMs[0], []string{"valor-indisponivel"})
	expected := []byte(output)

	test := struct {
		name     string
		bank     Bank
		input    Withdraw
		want     []byte
		wantFail bool
	}{
		name:     "Attempt to withdraw more cash than amount available should return error",
		bank:     bank,
		input:    input,
		want:     expected,
		wantFail: true,
	}

	t.Run(test.name, func(t *testing.T) {

		got := test.bank.Withdraw(test.input)
		equal := bytes.Equal(got, test.want)
		if !equal {
			t.Errorf("TestWithdraw_NotEnoughCashAvailable_ShouldReturnError result is not equal to expected")
		}
	})
}

func TestWithdraw_DoubleWithdraw_ShouldReturnError(t *testing.T) {
	bank := initBank(1)
	bank.ATMs[0].Cash = map[string]int{
		"notasDez":       100,
		"notasVinte":     50,
		"notasCinquenta": 10,
		"notasCem":       100,
	}
	bank.ATMs[0].Available = true

	lastWithdraw, _ := time.Parse("2006-01-02T15:04:05.000Z", "2022-04-01T10:10:00.000Z")
	bank.ATMs[0].LastWithdraw = LastWithdraw{
		Time:  lastWithdraw,
		Value: 50,
	}

	input, _ := getInputWithdraw(1, 50, 10, 19)

	output := formatOutput(bank.ATMs[0], []string{"saque-duplicado"})
	expected := []byte(output)

	test := struct {
		name     string
		bank     Bank
		input    Withdraw
		want     []byte
		wantFail bool
	}{
		name:     "Attempt to withdraw from non available ATM should return error",
		bank:     bank,
		input:    input,
		want:     expected,
		wantFail: true,
	}

	t.Run(test.name, func(t *testing.T) {

		got := test.bank.Withdraw(test.input)
		equal := bytes.Equal(got, test.want)
		if !equal {
			t.Errorf("TestWithdraw_DoubleWithdraw_ShouldReturnError result is not equal to expected")
		}
	})
}

func TestWithdraw_WithdrawCashValues_ShouldSucceed(t *testing.T) {
	bank := initBank(1)
	bank.ATMs[0].Cash = map[string]int{
		"notasDez":       100,
		"notasVinte":     50,
		"notasCinquenta": 10,
		"notasCem":       100,
	}
	bank.ATMs[0].Available = true

	lastWithdraw, _ := time.Parse("2006-01-02T15:04:05.000Z", "2022-04-01T10:10:00.000Z")
	bank.ATMs[0].LastWithdraw = LastWithdraw{
		Time:  lastWithdraw,
		Value: 50,
	}

	input, _ := getInputWithdraw(1, 60, 10, 20)

	atmAfter := bank.ATMs[0]
	atmAfter.Cash = map[string]int{
		"notasDez":       99,
		"notasVinte":     50,
		"notasCinquenta": 9,
		"notasCem":       100,
	}
	output := formatOutput(atmAfter, []string{})
	expected := []byte(output)

	test := struct {
		name     string
		bank     Bank
		input    Withdraw
		want     []byte
		wantFail bool
	}{
		name:     "Attempt to withdraw from non available ATM should return error",
		bank:     bank,
		input:    input,
		want:     expected,
		wantFail: true,
	}

	t.Run(test.name, func(t *testing.T) {

		amoutBefore := test.bank.ATMs[0].totalValue()
		got := test.bank.Withdraw(test.input)
		equal := bytes.Equal(got, test.want)
		if !equal {
			t.Errorf("TestWithdraw_WithdrawCashValues_ShouldSucceed result is not equal to expected")
		}

		if test.bank.ATMs[0].totalValue() != amoutBefore-input.Value {
			t.Errorf("TestWithdraw_WithdrawCashValues_ShouldSucceed ATM`s total cash %v does not match expected value after withdrawing %v", amoutBefore, input.Value)
		}
	})
}
